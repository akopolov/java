package CallGui;
import CallMath.*;
import Graf.*;
import MathCalc.*;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JToolTip;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;

/**
 * <p>Class that will make the GUI</p>
 * @author Aleksei Kopõlov
 *
 */

public class GuiFrame {
	private JFrame frame;
	private JTextField text;
	private JButton [][]numButtonsMatrix = new JButton[4][3];
	private int countW=5,bWidth=75,bHeight=40,offsetX=10,offsetY=bHeight+10,buttonsOffset=5,press=0;
	private String [] buttonsMath={"=","+","-","*","/","^","Sqrt","DEL","To M","Get M","(",")","C","OFF"};
	private String [] superButtons={"!","Ln","Sin","Cos","Tan","Cot"};
	private int countH =(1+(buttonsMath.length/2));
	private int width = 0;
	private int height = 0;
	private String memmory=null;
	Color fColor = new Color(192,192,192);
	Color bColor = new Color(160,160,160);
	private JButton [][]mathButtonsMatrix = new JButton[1+(buttonsMath.length/2)][2];
	private JButton [][]extraMathButtonsMatrix = new JButton[1+(superButtons.length/3)][3];
	
	
	
	public GuiFrame(){
		width = countW*(bWidth+buttonsOffset)+offsetX+buttonsOffset;
		height = countH*(bHeight+offsetX)+offsetX+bHeight;
		frame = new JFrame("Calculator");
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(450, 200, width, height);
		frame.getContentPane().setBackground(fColor);
		frame.getContentPane().setLayout(null);
		
		
		
		/**
		 * <p>Create number buttons (0-9) and buttons ("." and "+/-")</p>
		 * @author Aleksei Kopõlov
		 *
		 */
		NumberButtonAction Action = new NumberButtonAction();
		int countB=0;
		for(int y=0;y<=3;y++){
			for(int x=0;x<=2;x++){
				if (y==0){
					numButtonsMatrix[y][x]=new JButton(Integer.toString(countB));
				}else{
					numButtonsMatrix[y][x]=new JButton(Integer.toString(++countB));
				}
				numButtonsMatrix[y][x].setBounds(offsetX+(x*(bWidth+buttonsOffset)), height-(offsetY+(y*(bHeight+buttonsOffset))),bWidth, bHeight);
				numButtonsMatrix[y][x].setToolTipText("This is a button that will input: "+(countB));
				numButtonsMatrix[y][x].addActionListener(Action);
				frame.add(numButtonsMatrix[y][x]);
			}
		}		
		for(int i =0;i<=2;i++ ){
			if(i==0){
				numButtonsMatrix[0][i].setText(".");
				numButtonsMatrix[0][i].setToolTipText("This is a button that will input: .");
			}else if (i==1){
				numButtonsMatrix[0][i].setText("0");
				numButtonsMatrix[0][i].setToolTipText("This is a button that will input: "+0);
			}else{
				numButtonsMatrix[0][i].setText("+/-");
				numButtonsMatrix[0][i].setToolTipText("This is a button that will input: "+("+/-")+" NB! INPUT ONLY AFTER: ( ");
			}
		}
		
		
		
		/**
		 * <p>Create math buttons form a list (buttonsMath)</p>
		 * @author Aleksei Kopõlov
		 *
		 */
		int countM=0;
		MathButtonsAction MathAction = new MathButtonsAction();
		for (int i = 0; i<(1+(buttonsMath.length/2));i++){
			if(i==0||i==4){
				for (int j = 1;j<2;j++){
					mathButtonsMatrix[i][j]=new JButton(buttonsMath[countM]);
					mathButtonsMatrix[i][j].setBounds(offsetX+(3*(bWidth+buttonsOffset)),height-(offsetY+(i*(bHeight+buttonsOffset))),(bWidth*2)+buttonsOffset,bHeight);
					mathButtonsMatrix[i][j].addActionListener(MathAction);
					mathButtonsMatrix[i][j].setToolTipText("This is a math button : "+buttonsMath[countM]);
					frame.add(mathButtonsMatrix[i][j]);
					countM++;
					
				}
			}else{
				for (int j = 0;j<2;j++){
					if(countM<buttonsMath.length){
						mathButtonsMatrix[i][j]= new JButton(buttonsMath[countM]);
						mathButtonsMatrix[i][j].setBounds(offsetX+((3+j)*(bWidth+buttonsOffset)), height-(offsetY+(i*(bHeight+buttonsOffset))), bWidth, bHeight);
						mathButtonsMatrix[i][j].addActionListener(MathAction);
						frame.add(mathButtonsMatrix[i][j]);
						mathButtonsMatrix[i][j].setToolTipText("This is a math button : "+buttonsMath[countM]);
						countM++;
					}
				}
			}
		}
		
		
		
		/**
		 * <p>Create extra math buttons form a list (superButtons)</p>
		 * @author Aleksei Kopõlov
		 *
		 */
		SuperMathAction ExtraAction = new SuperMathAction();
		int countE=0;
		for(int i=0; i<=(superButtons.length/3);i++){
			for(int j = 0; j<3;j++){
				if(countE<superButtons.length){	
					extraMathButtonsMatrix[i][j] = new JButton(superButtons[countE]);
					extraMathButtonsMatrix[i][j].setBounds(offsetX+((j)*(bWidth+buttonsOffset)), height-(offsetY+((i+4)*(bHeight+buttonsOffset))), bWidth, bHeight);
					extraMathButtonsMatrix[i][j].addActionListener(ExtraAction);
					extraMathButtonsMatrix[i][j].setToolTipText("This is a math button : "+superButtons[countE]);
					frame.add(extraMathButtonsMatrix[i][j]);
					countE++;
				}
			}
		}
		
		
		
		/**
		 * <p>Create Inp/Out textfield</p>
		 * @author Aleksei Kopõlov
		 *
		 */
		EnterText TextAction = new EnterText();
		text = new JTextField();
		text.setBounds(offsetX, offsetX, width-(2*offsetX), bHeight);
		text.setText("");
		text.setEditable(false);
		text.requestFocusInWindow();
		text.addActionListener(TextAction);
		
		frame.add(text);
		
		
		frame.setResizable(false);
		frame.repaint();
		
	}
	
	
	
	/**
	 * <p>Number buttons event</p>
	 * @author Aleksei Kopõlov
	 *
	 */
	public class NumberButtonAction implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String inp = e.getActionCommand();
			if (inp=="+/-"){
				text.setText(new CheckMinPlus().CheckMinPlus(text.getText()));
			}else if(inp=="."){
				text.setText(new CheckDot().CheckDot(text.getText(),buttonsMath));
			}else{
				text.setText(new CheckNum().CheckNum(text.getText(),inp));
			}
		}
	}
	
	
	/**
	 * <p>math buttons event</p>
	 * @author Aleksei Kopõlov
	 *
	 */
	public class MathButtonsAction implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			switch (e.getActionCommand()){
				case("/"):
				case("*"):
				case("-"):
				case("^"):
				case("+"):text.setText(new CheckPMMD().CheckPluss(text.getText(),e.getActionCommand(),buttonsMath));
					break;
					
				case("C"):text.setText("");
					break;
					
				case("DEL"):text.setText(new CheckDEL().CheckDEL(text.getText(),superButtons));

					break;
				
				case("="):text.setText(new CheckInp().CheckInp(text.getText(),buttonsMath));
					break;
					
				case("("):text.setText(new CheckLeftCapt().CheckLeftCapt(text.getText(),buttonsMath));
					break;
					
				case(")"):text.setText(new CheckRightCapt().CheckRightCapt(text.getText(),buttonsMath));
					break;
					
				case("To M"):memmory=text.getText();
					break;
				
				case("Get M"):text.setText(memmory==null ? text.getText():memmory);
					break;
					
				case("OFF"):System.exit(0);
					break;
					
				case("Sqrt"):text.setText(new CheckSqrt().CheckSqrt(text.getText()));
					break;
					
				default:text.setText("Defualt");
					break;
			}
			
		}
	}
	
	
	
	/**
	 * <p>Enter event</p>
	 * @author Aleksei Kopõlov
	 *
	 */
	public class EnterText implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			new End();//START UP THE DUNAMIC GRAF
		}
	}
	
	/**
	 * <p>Execute calculation event</p>
	 * @author Aleksei Kopõlov
	 *
	 */
	public class SuperMathAction implements ActionListener{
		public void actionPerformed(ActionEvent e){
			text.setText(new CheckSuperMath().CheckSuperMath(text.getText(),e.getActionCommand()));

		}
	}
}
