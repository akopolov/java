package CallMath;

import javax.swing.JOptionPane;
/**
 * <p>Validating user input.</p>
 * @author Aleksei Kopõlov
 *
 */
public class CheckInp {
	public String CheckInp(String inp,String math[]){
		String out = null; 
		String help=null;
		int countC=0;
		boolean start=true;
	
		if(inp.length()>0)
		{
			for (String x : math)
			{
				if(String.valueOf(inp.charAt(inp.length()-1)).equals(")"))
				{
					
					break;
					
				}else if(String.valueOf(inp.charAt(inp.length()-1)).equals(x))
				{
					
					start=false;
					help="Last input Error: "+inp.charAt(inp.length()-1)+" ?";
					
				}else if(String.valueOf(inp.charAt(inp.length()-1)).equals("."))
				{
					
					start=false;
					help="Last input Error: "+inp.charAt(inp.length()-1)+" ?";
					
				}
			}
			
			
			
			for(int i = 0 ; i<inp.length();i++)
			{
				
				if(String.valueOf(inp.charAt(i)).equals("("))
				{
				
					countC+=1;
					
				}else if(String.valueOf(inp.charAt(i)).equals(")"))
				{
					
					countC-=1;
					
				}else if(String.valueOf(inp.charAt(i)).equals("="))
				{
					
					start=false;
					help="Why is there a ' = ' in your text box ? Delete It";
					
				}
				
			}
			
			
			
			if(countC>0)
			{
				
				start=false;
				help="Check the number of brackets  ')' ";
				
			}
			
			
			
			if(start==false)
			{
				
				out=inp;
				JOptionPane.showMessageDialog(null, help);
				
			}else
			{
				
				out=inp+"="+new Read().Read(inp, math);
				
			}
		}else
		{
			
			out=inp;
			
		}
			
		return out;
		
	}
}
