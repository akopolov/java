package CallMath;

/**
 * <p>This class does MATH</p>
 * @author Aleksei Kopõlov
 *
 */

public class DoMath {
	
	public String DoMath(String algorithm[][]){
		String out=null;
		
		
		out=DoIt(algorithm, algorithm.length-1, 0,false);
		
		return out;
		
	}
	
	/**
	 * <p>Constructor that dose all the mathematical operations </p>
	 * @author Aleksei Kopõlov
	 *
	 */
	public String DoIt(String algorithm[][],int y,int x,boolean can){
		
		double fixt=1000000.0;
		
			if(String.valueOf(algorithm[y][x]).equals("*"))
			{

				if(String.valueOf(algorithm[y][x-1]).equals("null")||
				   String.valueOf(algorithm[y][x+1]).equals("null"))
				{

					DoIt(new Numlook().Numlook(algorithm, y, x),y,x,can);
					
				}else
				{
					
				algorithm[y][x-1]=String.valueOf(Double.parseDouble(algorithm[y][x-1])*Double.parseDouble(algorithm[y][x+1]));
				algorithm[y][x]=null;
				algorithm[y][x+1]=null;
				DoIt(algorithm,y,x+1,can);
				
				}
			
			}else if(String.valueOf(algorithm[y][x]).equals("^"))
			{
				
				if(String.valueOf(algorithm[y][x-1]).equals("null")||
				   String.valueOf(algorithm[y][x+1]).equals("null"))
				{

					DoIt(new Numlook().Numlook(algorithm, y, x),y,x,can);
									
				}else
				{
						
					algorithm[y][x-1]=String.valueOf(Math.pow(Double.parseDouble(algorithm[y][x-1]),Double.parseDouble(algorithm[y][x+1])));
					algorithm[y][x]=null;
					algorithm[y][x+1]=null;
					DoIt(algorithm,y,x+1,can);
								
				}
				
			}else if(String.valueOf(algorithm[y][x]).equals("/"))
			{
				
				if(String.valueOf(algorithm[y][x-1]).equals("null")||
				   String.valueOf(algorithm[y][x+1]).equals("null"))
				{

					DoIt(new Numlook().Numlook(algorithm, y, x),y,x,can);
							
				}else
				{
							
					if(Double.parseDouble(algorithm[y][x+1])==0)
					{
						
						return algorithm[0][0]="Infinity";
						
					}else
					{
						
					algorithm[y][x-1]=String.valueOf(Double.parseDouble(algorithm[y][x-1])/Double.parseDouble(algorithm[y][x+1]));
					algorithm[y][x]=null;
					algorithm[y][x+1]=null;
					DoIt(algorithm,y,x+1,can);
					
					}	
					
				}
	
			}else if(String.valueOf(algorithm[y][x]).equals("+") && can==true)
			{
				
				if(String.valueOf(algorithm[y][x-1]).equals("null")||
				   String.valueOf(algorithm[y][x+1]).equals("null"))
				{

					DoIt(new Numlook().Numlook(algorithm, y, x),y,x,true);
									
				}else
				{
							
					algorithm[y][x-1]=String.valueOf(Double.parseDouble(algorithm[y][x-1])+Double.parseDouble(algorithm[y][x+1]));
					algorithm[y][x]=null;
					algorithm[y][x+1]=null;
					DoIt(algorithm, y, x+1,true);
					
				}
							
			}else if(String.valueOf(algorithm[y][x]).equals("-") && can==true)
			{
				
				if(String.valueOf(algorithm[y][x-1]).equals("null")||
				   String.valueOf(algorithm[y][x+1]).equals("null"))
				{

					DoIt(new Numlook().Numlook(algorithm, y, x),y,x,true);
									
				}else
				{
							
					algorithm[y][x-1]=String.valueOf(Double.parseDouble(algorithm[y][x-1])-Double.parseDouble(algorithm[y][x+1]));
					algorithm[y][x]=null;
					algorithm[y][x+1]=null;
					DoIt(algorithm, y, x+1,true);
					
				}
				
			}else
			{
				
				if(x+1<algorithm[y].length){

					DoIt(algorithm, y, x+1,can);
					
				}else if(x+1==algorithm[y].length)
				{
					
					for(int i= 0; i<algorithm[y].length;i++)
					{
						
						if(String.valueOf(algorithm[y][i]).equals("+")||
						   String.valueOf(algorithm[y][i]).equals("-"))
						{
						
							DoIt(algorithm, y, 0, true);
						
						}
						
					}
					
					for (int j = 0; j<algorithm[y].length;j++)
					{
						if (y>0){
						
							if(String.valueOf(algorithm[y][j]).equals("null"))
							{
								
								
								
							}else
							{
								
								algorithm[y-1][j]=algorithm[y][j];
								algorithm[y][j]=null;
								
							}
							
						}else
						{
							if(String.valueOf(algorithm[y][j]).equals("null"))
							{
								
								
								
							}else
							{
							
								algorithm[0][0]=algorithm[y][j];
								
							}
							
						}
						
					}
					
					if (y>0)
					{
						
						DoIt(algorithm,y-1,0,false);
					
					}
					
				}
				
			}
//		return algorithm[0][0];
		return String.valueOf(Math.round(Double.parseDouble(algorithm[0][0])*fixt)/fixt);
		
	}
}
