package CallMath;

import javax.swing.JOptionPane;

/**
 * <p>Constructing algorithm hierarchy.</p>
 * @author Aleksei Kopõlov
 *
 */

public class Read {
	public String Read(String inp,String math[]){
		String out=null;
		int countMW=0;
		int countMH=1;
		
			for(int i=0;i<inp.length();i++)
			{
			
				if(String.valueOf(inp.charAt(i)).equals("+")||
				  (String.valueOf(inp.charAt(i)).equals("-")&&
						  !(String.valueOf(inp.charAt(i-1)).equals("(")))||
				   String.valueOf(inp.charAt(i)).equals("*")||
				   String.valueOf(inp.charAt(i)).equals("/")||
				   String.valueOf(inp.charAt(i)).equals("^")||
				   String.valueOf(inp.charAt(i)).equals("+"))
				{
					
					countMW++;
					
				}
				
				if(String.valueOf(inp.charAt(i)).equals("("))
				{
					
					
					for(int j=i+1;j<inp.length();j++)
					{
						
						if(String.valueOf(inp.charAt(j)).equals("("))
						{
							
							countMH++;
							break;
							
						}else if(String.valueOf(inp.charAt(j)).equals(")"))
						{

							break;
							
							
						}
						
					}
					
				}
			
			}
			
			
			
			String algorithm [][]= new String [countMH+1][(countMW*2)+1];
			int x=0,y=0,start=0;
			boolean can=true;
			
			for(int i=0; i<inp.length();i++)
			{
			
					if(String.valueOf(inp.charAt(i)).equals("+")||
					  (String.valueOf(inp.charAt(i)).equals("-")&&
							  !(String.valueOf(inp.charAt(i-1)).equals("("))))
					{
						if (can==true)
						{
					
							algorithm[y][x]=String.valueOf(inp.substring(start,i));
							algorithm[y][++x]=String.valueOf(inp.charAt(i));
							x++;
							start=i+1;
							
						}else
						{
						
							algorithm[y][++x]=String.valueOf(inp.charAt(i));
							x++;
							start=i+1;
							can=true;
							
						}
						
					}else if(String.valueOf(inp.charAt(i)).equals("*")||
							 String.valueOf(inp.charAt(i)).equals("/")||
							 String.valueOf(inp.charAt(i)).equals("^"))
					{
						
						if(can==true)
						{
						
							algorithm[y][x]=String.valueOf(inp.substring(start, i));
							algorithm[y][++x]=String.valueOf(inp.charAt(i));
							x++;
							start=i+1;
							
						}else
						{
							
							algorithm[y][++x]=String.valueOf(inp.charAt(i));
							x++;
							start=i+1;
							can=true;
							
						}
						
					}else if(String.valueOf(inp.charAt(i)).equals("("))
					{
						
						start=i+1;
						y++;
					
					}else if(String.valueOf(inp.charAt(i)).equals(")"))
					{
						
						if(String.valueOf(inp.charAt(i)).equals(")")&&
						   String.valueOf(inp.charAt(i-1)).equals(")"))
						{
							
							can=false;
							y--;
							
						}else
						{
							
							algorithm[y][x]=String.valueOf(inp.substring(start,i));
							can=false;
							y--;	
							
						}
						
					}else if(i==inp.length()-1)
					{
						
						algorithm[y][x]=String.valueOf(inp.substring(start, i+1));
						
					}
					
			}
			
			out = new DoMath().DoMath(algorithm);
			
			if(String.valueOf(out).equals("NaN"))
			{
				
				JOptionPane.showMessageDialog(null, "some bug");
				out="0";
				
			}else if (String.valueOf(out).equals("Infinity"))
			{
				
				JOptionPane.showMessageDialog(null, "You are dividing by 0. You cant do that. Unless you are very good at MATH. In witch case WHY ARE YOU USING THIS thing?");
				out="0";
				
			}
			
		return out;
	}
}

