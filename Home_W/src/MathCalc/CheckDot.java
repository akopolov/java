package MathCalc;
/**
 * <p>How and when to execute dot button.</p>
 * @author Aleksei Kopõlov
 *
 */
public class CheckDot{
	public String CheckDot(String inp,String math[]){
		String out ;
		boolean CanPut = true;
		
		if(inp.length()==0)
		{
			
			CanPut = false;
			
		}
		
		for(int i=0;i<inp.length();i++)
		{
			
			if((String.valueOf(inp.charAt(i))).equals("."))
			{
				
				CanPut=false;
				
			}else if((String.valueOf(inp.charAt(i))).equals("+")||
					 (String.valueOf(inp.charAt(i))).equals("-")||
					 (String.valueOf(inp.charAt(i))).equals("*")||
					 (String.valueOf(inp.charAt(i))).equals("^")||
					 (String.valueOf(inp.charAt(i))).equals("/"))
			{
				
				CanPut=true;
				
			}
		}
		if (inp.length()>0){
			for(String x: math){
				if(String.valueOf(inp.charAt(inp.length()-1)).equals(x))
				{
					CanPut = false;
				}
			}
		}
		
		if(CanPut==true){
			
			out=inp+".";
			
		}else{
			
			out=inp;
			
		}
		return out;
	}

}
