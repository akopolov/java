package MathCalc;
/**
 * <p>How and when to execute delete button.</p>
 * @author Aleksei Kopõlov
 *
 */
public class CheckDEL {
	public String CheckDEL(String inp,String []Math){
		String out=null;
		boolean IsMath = false;
		int SetW = 0;
		String Sqrt="Sqrt(";
		
		if (inp.length()>0)
		{
			
			for(int i=0;i < Math.length;i++)
			{
				
				String test=Math[i]+"(";
				int width = test.length();
				
				if(inp.length()<width)
				{
				
					out=inp.substring(0,inp.length()-1);
				
				}else
				{
			
					if(inp.substring(inp.length()-width, inp.length()).equals(test))
					{
						
						IsMath=true;
						SetW=width;
						break;
						
					}else
					{
						
						out=inp.substring(0,inp.length()-1);
						
					}
				}
			}

			
			if (inp.length()>=Sqrt.length())
			{
				if(inp.substring(inp.length()-Sqrt.length(),inp.length()).equals(Sqrt))
				{
					
					IsMath=true;
					SetW=Sqrt.length();
					
				}else
				{
					
					out=inp.substring(0,inp.length()-1);
					
				}
			}
			
			
			if(IsMath==true){
				
				out=inp.substring(0, inp.length()-SetW);
				
			}
			
		}else
		{
			
			out=inp;
			
		}
		
		return out;
	}
}
