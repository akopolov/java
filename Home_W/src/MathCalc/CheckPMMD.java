package MathCalc;
/**
 * <p>How and when to execute + , - , ^ , * , / buttons.</p>
 * @author Aleksei Kopõlov
 *
 */
public class CheckPMMD {
	public String CheckPluss(String InpText,String command,String [] Mathbuttons){
		String out=null;
		
		if (InpText.length()==0){
			out=InpText;
		}
		
		if(InpText.length()>0){
		
			if(String.valueOf(InpText.charAt(InpText.length()-1)).equals(command)||
			   String.valueOf(InpText.charAt(InpText.length()-1)).equals(".")||
			   String.valueOf(InpText.charAt(InpText.length()-1)).equals("(")||
			   String.valueOf(InpText.charAt(InpText.length()-1)).equals("="))
			{
				
				out=InpText;
				
			}else if (String.valueOf(InpText.charAt(InpText.length()-1)).equals("-")||
					  String.valueOf(InpText.charAt(InpText.length()-1)).equals("+")||
					  String.valueOf(InpText.charAt(InpText.length()-1)).equals("*")||
					  String.valueOf(InpText.charAt(InpText.length()-1)).equals("^")||
					  String.valueOf(InpText.charAt(InpText.length()-1)).equals("/"))
			{
				
				String newInp=InpText.substring(0, InpText.length()-1);
				out=newInp+command;
				
			}else{
				
				out=InpText+command;
				
			}
			
		}
	return out;
	}

}
