package MathCalc;
/**
 * <p>How and when to execute left bracket button.</p>
 * @author Aleksei Kopõlov
 *
 */
public class CheckLeftCapt {
	public String CheckLeftCapt(String inp,String [] MathButton){
		String out="(";
		
		if(inp.length()>0)
		{
			if(String.valueOf(inp.charAt((inp.length())-1)).equals("."))
			{
				
				out=inp;
				
			}else if(String.valueOf(inp.charAt((inp.length())-1)).equals("+")||
					 String.valueOf(inp.charAt((inp.length())-1)).equals("-")||
					 String.valueOf(inp.charAt((inp.length())-1)).equals("*")||
					 String.valueOf(inp.charAt((inp.length())-1)).equals("/")||
					 String.valueOf(inp.charAt((inp.length())-1)).equals("(")||
					 String.valueOf(inp.charAt((inp.length())-1)).equals("^")||
					 String.valueOf(inp.charAt((inp.length())-1)).equals("="))
			{
				
				out=inp+"(";
				
			}else{
				
				out=inp+"*(";
				
			}
		}
		return out;
	}
}
