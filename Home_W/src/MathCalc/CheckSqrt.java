package MathCalc;
/**
 * <p>How and when to execute Sqrt button.</p>
 * @author Aleksei Kopõlov
 *
 */
public class CheckSqrt {
	
	public String CheckSqrt(String inp){
		String out=null;
		
		if(inp.length()>0)
		{
			Character last=inp.charAt(inp.length()-1);
			if(Character.isDigit(last)||
			   String.valueOf(last).equals(".")||
			   String.valueOf(last).equals(")"))
			{
				
				out=inp;
				
			}else{
				
				out=inp+"Sqrt(";
				
			}
			
		}else
		{
			
			out=inp+"Sqrt(";
			
		}
		return out;
	}
}
