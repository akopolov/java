package MathCalc;
/**
 * <p>How and when to execute buttons from 0 to 9.</p>
 * @author Aleksei Kopõlov
 *
 */
public class CheckNum {
	public String CheckNum(String inp,String command){
		String out=null;
		
		if(inp.length()>0){
			if(String.valueOf(inp.charAt(inp.length()-1)).equals(")")){
					
					out=inp+"*"+command;
					
			}else{
					
					out=inp+command;
					
			}
		
			
		}else
		{
			
			out=command;
			
		}
		
		return out;
	}

}
