package MathCalc;

import javax.sound.midi.SysexMessage;
/**
 * <p>How and when to execute right bracket button.</p>
 * @author Aleksei Kopõlov
 *
 */
public class CheckRightCapt {
	public String CheckRightCapt(String inp,String []Mathbuttons){
		String out = null;
		boolean canput=false;
		boolean count=false;
		int left=0,right=0;
		
		for(int i=0;i<inp.length();i++)
		{
			
			if((String.valueOf(inp.charAt(i))).equals("("))
			{
				
				left++;

			}else if((String.valueOf(inp.charAt(i))).equals(")"))
			{
				
				right++;
				
			}
		}
		
		
		//CHECK THE NUMBER OF "(" AND ")"
		if(left==0||
		   left==right)
		{
		
			count=false;
			
		}else
		{
			
			count=true;
			
		}
		
		
		
		if(inp.length()!=0)
		{
			if((String.valueOf(inp.charAt((inp.length())-1)).equals("-")||
			    String.valueOf(inp.charAt((inp.length())-1)).equals("+")||
		   		String.valueOf(inp.charAt((inp.length())-1)).equals("*")||
				String.valueOf(inp.charAt((inp.length())-1)).equals("/")||
			    String.valueOf(inp.charAt((inp.length())-1)).equals("(")||
			    String.valueOf(inp.charAt((inp.length())-1)).equals("^")||
				String.valueOf(inp.charAt((inp.length())-1)).equals(".")))
			{
			
				canput=false;
				
			}else{
				
				canput=true;
				
			}
		}

		
		
		
		//OUT FALSE OR TRUE
		if(canput==true&&
		   count==true)
		{
			
			out=inp+")";
			
		}else
		{
			
			out=inp;
			
		}
		
		return out;
		
	}
}
