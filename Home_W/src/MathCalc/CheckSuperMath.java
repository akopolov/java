package MathCalc;
/**
 * <p>How and when to execute ANY button from superButtons list.</p>
 * @author Aleksei Kopõlov
 *
 */
public class CheckSuperMath {
	public String CheckSuperMath(String inp,String command){
		String out = null;
		
		if(inp.length()>0){
			
			char last = inp.charAt(inp.length()-1);
			if(Character.isDigit(last)||
			   String.valueOf(last).equals(".")||
			   String.valueOf(last).equals(")"))
			   
			{
			
				out=inp;
				
			}else
			{
				
				out=inp+command+"(";
				
			}
			
		}else
		{
			
			out=inp+command+"(";
			
		}
		return out;
	}
}
