package MathCalc;
/**
 * <p>How and when to execute +/- button.</p>
 * @author Aleksei Kopõlov
 *
 */
public class CheckMinPlus {
	public String CheckMinPlus(String inp){
		String out=null;
		
		if(inp.length()>0)
		{
			
			if(String.valueOf(inp.charAt(inp.length()-1)).equals("("))
			{
			
				out=inp+"-";
				
			}else if(String.valueOf(inp.charAt(inp.length()-1)).equals("-"))
			{
				
				out=inp.substring(0, inp.length()-1);
				
			}else
			{
				
				out=inp;
				
			}
			
		}else
		{
			
			out=inp;
			
		}
		
		return out ;
	}

}
