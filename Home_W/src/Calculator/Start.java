package Calculator;

import CallGui.GuiFrame;
/**
 * <p>Start up class</p>
 * @author Aleksei Kopõlov
 *
 */
public class Start {
	public static void main(String[] args) {
		new GuiFrame();
	}
}
