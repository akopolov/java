package Graf;

import java.awt.Color;
import java.awt.Graphics;
import java.applet.Applet;

public class StatoXoY extends Applet {
	int set = 20;
	
	public void paint(Graphics g){
	    int w=getWidth();
	    int h=getHeight();
		
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, w, h);
        
        g.setColor(Color.BLACK);
        g.drawLine(0,h/2, w, h/2);
        g.drawLine(w/2, 0, w/2, h);
        
        for(int i = (w/2)+(w/set) ; i<w;i+=w/set){
        	g.drawLine(i, (h/2)+5, i, (h/2)-5);
        	g.drawString(Integer.toString((i-(w/2))/(w/set)), i-3, (h/2)+20);
        	g.drawLine(w-i,(h/2)+5, w-i, (h/2)-5);
        	g.drawString(Integer.toString(-(i-(w/2))/(w/set)), w-i-6, (h/2)+20);
        }
        for(int i =(h/2)+(h/set);i<h;i+=h/set){
        	g.drawLine((w/2)+5, i, (w/2)-5, i);
        	g.drawString(Integer.toString((i-(h/2))/(h/set)), (w/2)-20,i+5 );
        	g.drawLine((w/2)+5, h-i, (w/2)-5, h-i);
        	g.drawString(Integer.toString(-(i-(h/2))/(h/set)), (w/2)-20,h-i+5 );
        }
	}
}
