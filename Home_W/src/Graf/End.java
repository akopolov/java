package Graf;
import javax.swing.*;
/**
 * <p>Class that will make a frame for the graph.</p>
 * @author Aleksei Kopõlov
 *
 */
public class End {
	public End(){
		JFrame f= new JFrame("Graf");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		DunamoXoY DXY= new DunamoXoY();
		f.add(DXY);
		f.setVisible(true);
		f.setBounds(200,200,200,200);
	}

}
