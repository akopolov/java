package Graf;
import java.awt.*;
import javax.swing.JPanel;
/**
 * <p>Class that will make a dynamic graph</p>
 * @author Aleksei Kopõlov
 *
 */
public class DunamoXoY extends JPanel{
	int set = 30;
	
	public void paintComponent(Graphics g) {
		int w=getWidth();
	    int h=getHeight();
	    int Line=5;
		
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, w, h);
        
        g.setColor(Color.BLACK);
        g.drawLine(0,h/2, w, h/2);
        g.drawLine(w/2, 0, w/2, h);
        
        for(int i =(w/2)+set;i<w;i+=set){//OX
        	g.drawLine(w-i, (h/2)-Line, w-i, (h/2)+Line);
        	g.drawString(Integer.toString(((w/2)-i)/set), w-i-7, (h/2)+21);
        	g.drawLine(i, (h/2)-Line, i, (h/2)+Line);
        	g.drawString(Integer.toString((i-(w/2))/set), i-3, (h/2)+21);
        }
        
        for(int i =(h/2)+set;i<h;i+=set){//OY
        	g.drawLine((w/2)-Line, h-i, (w/2)+Line, h-i);
        	g.drawString(Integer.toString((i-(h/2))/set), (w/2)-21, h-i+5);
        	g.drawLine((w/2)-Line, i, (w/2)+Line, i);
        	g.drawString(Integer.toString(((h/2)-i)/set), (w/2)-25, i+5);
        }
	}
}
